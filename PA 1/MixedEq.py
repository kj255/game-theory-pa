import numpy as np
import sys

from Game import Game, Player, read_file, read_payoff, SolveForMixed, makeGame

def main(argv):
	game,Player1,Player2 = makeGame(argv)
	mixed_eq = game.Mixed_Eq()
	for args in mixed_eq:
		print(*args)


if __name__ == '__main__':
	main(sys.argv)