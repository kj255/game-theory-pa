import numpy as np
import sys

from Game import Game, Player, read_file, read_payoff, SolveForMixed, makeGame

def main(argv):
	game,Player1,Player2 = makeGame(argv)
	player_no = input('Enter the Player Number (1 or 2)')
	if player_no == '1':
		player = Player1
	else:
		player = Player2
	dom,compare = player.dominated()
	for a,b in compare:
		print(a,b)


if __name__ == '__main__':
	main(sys.argv)